/* Config file for Parse Server instance. Here you can edit all the configuration variables of 
the server. */

module.exports = {
    IP: 'mongodb://localhost:27017/bernardelli',
    cloud_code: 'cloud/cloud_code.js',
    appId: 'Bernardelli Assicurazioni',
    masterKey: '1234567',
    URL: 'http://138.68.100.112:1337/parse',
    port: '1337',
    name: 'Bernardelli Assicurazioni',
    dashboard_port: '4040',
    user: 'Patrizio',
    pass: 'staffonly',
    readOnly: false,
    apps: [{'appId': 'Bernardelli Assicurazioni'}]
}