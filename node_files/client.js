//Principal web-app js page


/* requires block */
let  Parse = require('parse/node');
let  config = require('./config_client'); // <- client configuration
let  express = require('express'); // i needed to re-declare express because i have to make REST API in this file
let  cors = require('cors');
let  bodyParser = require('body-parser');

/* declaration block */

let  app = express();
app.use(bodyParser.urlencoded({ limit:'50mb', extended: false }));
app.use(bodyParser.json({limit:'50mb'}));
app.use(cors());
/* The code starts here 
// INITIALIZING PARSE CONNECTION INSTANCE */

Parse.initialize(config.appId);
Parse.serverURL = config.serverURL;
Parse.masterKey = config.masterKey;
Parse.Cloud.useMasterKey();

/* REST API Endpoints here - */

/* Signup endpoint, POST request with the params showed below. In case, I will create a little 
documentation with all the params needed for the request.
Anyway, this function should be already tested when the brief will come, so I will just add some 
params.*/
app.post('/user/signup', function(req, res){
    console.log(req.body['nome']);
    let  name = req.body['nome'];
    let  surname = req.body['cognome'];
    let  phone = req.body['telefono'];
    let  taxNumber = req.body['IVA'];
    let  birthday = new Date(req.body['data_nascita']);
    let  email = req.body['email'];
    let  password = req.body['password'];
    let  subscribedNews = req.body['news']; //this is for the agreement to newsletter
    let  dataProcessing = req.body['dataProcessing']; //this is for the agreement to data processing
    let  dataCommunication = req.body['dataCommunication']; //this is for the agreement to third-party data communication
    let  role = req.body['role']; //this is for user role, Operator or End User

    let  user = new Parse.User();
    
    user.set("username", email); //the username will be the e-mail, or unique-generated SHA code(maybe with 6-7 characters slip)
    user.set("password", password);
    user.set("email", email);
    user.set("name", name);
    user.set("surname", surname);
    user.set("phone", phone);
    user.set("taxNumber", taxNumber);
    user.set("birthday", birthday);
    user.set("subscribedNews", subscribedNews);
    user.set("dataProcessing", dataProcessing);
    user.set("dataCommunication", dataCommunication);
    user.set("role", role);
    
    console.log(JSON.stringify(user));
    try{
        user.signUp({useMasterKey:true}).then((obj)=>{
            console.log("Registrazione effettuata con successo " + obj);
            res.send(JSON.stringify(obj));
        }); // all those calls in Parse API will be done asynchronously
        //need to make redirection
    }catch(parseError){
        console.log("Error: " + parseError.code + " " + parseError.message);
    }
});

/* 
......./////////////////////////          
           END SIGNUP
......./////////////////////////
*/


/* Login API endpoint
Same discourse as signup endpoint.
I will then add in a little documentation the calls to make in POST */

app.post('/user/login', function(req, res){
    let  email = req.body.email;
    let  password = req.body.password;
    Parse.User.logIn(email, password).then((result)=>{
        res.send('Login effettuato con successo');
        res.send(true);
    }, (err)=>{
        res.send('Errore di login');
        res.send(false);
    });
    //need to make redirection
    
});

/* Lately I will make persistance recognizing session Id if the Parse.CurrentUser() won't work.
To do this, i will add two more packages: cookie-parse and cookie-session.
*/




/* Logout API endpoint */

app.get('/user/logout', function(req, res){
    Parse.User.logOut().then(() => {
          let  user = Parse.User.current(); //setting the cached user to null
          //needs redirection
    });
});

/* Get best Insurance API endpoint 

Need to follow up the sequent schema:

 SCHEMA:
        {
            id:
            {
                [FATTURATO]:
                {
                    [MASSIMALE]: [PREMIO]
                }
            
            }
        }

        y = Fatturato
        x = Massimale

*/

app.post('/insurance/getBestInsurance', function(req, res){

     console.log("Payload di richiesta:")
     console.log(req.body);

     let earnings = req.body['fatturato'];
     let maximal = req.body['massimale'];
     let job = req.body['professione'];
     let cat = req.body['categoria'];
     let visto = req.body['visto'];

     let  InsurancesQuery = new Parse.Query('Insurances');
     let  insuranceArray = [];

     InsurancesQuery.equalTo('tipo', job);

     InsurancesQuery.find().then((Insurances)=>{
           
           for(let  i = 0; i < Insurances.length; i++){

                let  payload = Insurances[i].get('payload');
                let  name = Insurances[i].get('nome');
                let  objectId = Insurances[i].id;
                let  files = Insurances[i].get('files');
                if(typeof payload[cat][earnings] === "object"){

                    if(typeof payload[cat][earnings][maximal] === "number"){
                        console.log(name);
                        console.log(payload[cat][earnings][maximal]);
                        
                        let  resPayload = {};
                        resPayload['name'] = name;
                        resPayload['premio'] = payload['0'][earnings][maximal];
                        if(professioneUtente === "Commercialista" && visto){
                            resPayload['premio'] += Insurances[i].get('visto_conformita');
                        }
                        resPayload['objectId'] = objectId;
                        resPayload['files'] = files;
                        insuranceArray.push(resPayload);

                    }
                }
                
                
           }

           res.send(JSON.stringify(insuranceArray));
     });

});

app.post('/insurance/createFile', function(req,res){

    const name = req.body['name'];
    const file = req.body['file'];
    const extension = req.body['extension'];
    let fileName = Math.floor(Math.random() * (1000000000-1)+1).toString(16) + "." + extension;
    console.log(name);
    console.log(extension);
    console.log(fileName);
    let parseFile = new Parse.File(fileName, { base64: file});
    parseFile.save().then((fileSaved)=>{

        let objectId = req.body['id'];
        let User = new Parse.Query(Parse.User);
        User.get(objectId).then((user)=>{
            user.set(name, fileSaved);
            user.save({useMasterKey: true}).then((updated)=>{
                console.log("File aggiunto con successo");
                res.sendStatus(200);
            });
        })

    }, (err)=>{
        console.log(err);
    });
});

app.post('/insurance/createRequestedInsurance', function(req, res){

   let id = req.body['id'];
   let insuranceId = req.body['insuranceId'];

   let UserQuery = new Parse.Query(Parse.User);
   UserQuery.get(id).then((user)=>{

       let InsuranceQuery = new Parse.Query('Insurances');
       InsuranceQuery.get(insuranceId).then((insurance)=>{

           let RequestInsurance = Parse.Object.extend('requestedInsurances');
           requestInsurance = new RequestInsurance();
           requestInsurance.set('insurance', insurance);
           requestInsurance.set('user', user);
           requestInsurance.save().then((obj)=>{

                console.log(obj);
                res.sendStatus(200);

           }, (err)=>{

               console.log(err);

           });

       });

   });
});

app.listen(3434, function(res){

});