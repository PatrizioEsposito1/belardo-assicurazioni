# Belardo Assicurazioni

Belardo Assicurazioni, job for Emmemedia sas.
Repo started on Friday, 26/10/2018 at 15:36

# Stack Used

* Express for routing Node.js and REST API calls
* MongoDB as a database, because we needed to make key->value pairs as the project requires it
* Facebook's Parse framework for Mongo, for a better usage of classes in our webapp
* React as front-end UI development main library
* MAYBE - persist.js for localStorage/ObjectStorage data persistance.
* Node.js(of course)

# Languages Used

* HTML5 (Of course)
* JavaScript (Of course pt.2)

The basic concept of this app is to create a better version of Facile.it and allow the end-user to have the best insurance plan for his needs.
In order to do this, we have to manage a lot of parameters - which in a standard LAMP stack or classic ASP can be a **real** problem.
So in our conference we decided Mongo- with Parse on top of it- is the better solution for this problem, as it is a non-relational database, and it gives us the possibility to have a better management of Objects and Classes.
The final result that i want to achieve is to retrieve all the Objects from the class "Insurance" with some filters defined by the users; this surely can be done using Cloud Code, which allows us to filter the data before we send it to the end user front-end display, using server-side computing.

# Basic documentation

In order to make easier the integration between React.js and Node.js, I will add there a little documentation for the call to make in AJAX.
For every change you need, please just ask directly through telegram, I'll answer ASAP.

## Signup - POST 

**Route: /user/signup **

**Params:**

* email
* password
* name
* surname
* phone
* taxNumber
* birthday
* subscribedNews
* dataProcessing
* dataCommunication
* role
 
**Little notes:**

* Tax number is Partita IVA
* dataProcessing is "Acconsento al trattamento dei dati personali a fini commerciali" et simila
* dataCommunication is "Acconsento alla comunicazioni dei dati personali a terze parti" et simila
* Role is an integer value: 0 = End User | 1 = Operator

## Login - POST

**Route: /user/login **
**Params:**

* email
* password

**Little notes:**

* I need to ask if the username will be a unique key or email. Meanwhile, I'll use e-mail
* I will make(maybe) a password encryption in MD5 for user protection.

## Logout - GET

**Route: /user/logout **

# EXTRA:

## Json format:

In order to get a clear view of how the data retrieving should be, I'll post a tiny json example.

```javascript
{
    "Professioni_Amministrative":{
        "Base":{
            "500.000":
        [
            {"15.000": "736,14"},
            {"30.000": "970,59"},
            {"50.000": "970,59"}
        ],
        "1.000.000":
        [
            {"15.000": "797,24"},
            {"30.000": "927,05"},
            {"50.000": "1051,15"}
        ]
        },
        "Coefficienti":
        {
            "Eta":
            [
                {
                    "21":"0,349",
                    "22":"0,356",
                    "23":"0,363",
                    "24":"0,370",
                    "25":"0,378"
                }
            ],
            "Provincia":
            [
                {
                    "AG":"0,956",
                    "AL":"1,008",
                    "AN":"0,967",
                    "AO":"1,055",
                    "AP":"0,937",
                    "AQ":"0,988",
                    "AR":"1,013"
                }
            ]
        },
        "Condizioni_Facoltative":
        {
            "Incarichi di sindaco, componente dell'organismo di vigilanza, di revisore legale dei conti":
            {
                "Premi":
                {
                    "500.000": ["450", "1350"],
                    "1.000.000": ["576", "1728"],
                    "1.500.000": ["648", "1944"]
                }
            },
            "FUNZIONI CONNESSE ALLE PROCEDURE CONCORSUALI PREVISTE DALLA LEGGE FALLIMENTARE, INCARICHI DI ATTESTATORE DEI PIANI DI RIENTRO, INCARICHI DI GESTORE DELLA CRISI DA SOVRAINDEBITAMENTO":
            {
                "Premi": "20% base"
            },
            "INCARICHI DI GIUDICE DI PACE, GIUDICE TRIBUTARIO, MEMBRO DI COMMISSIONE TRIBUTARIA":{
                "Premi": "5% base"
            },
            "RILASCIO DI VISTO DI CONFORMITÀ (ESCLUSO MOD. 730), CERTIFICAZIONE TRIBUTARIA E ASSEVERAZIONE DI STUDI DI SETTORE":
            {
                "Premi": {
                    "15.000": "233",
                    "30.000": "271",
                    "50.000": "307",
                    "70.000": "367"
                }
            },
            "RILASCIO DI VISTO DI CONFORMITÀ (COMPRESO MOD. 730), CERTIFICAZIONE TRIBUTARIA E ASSEVERAZIONE DI STUDI DI SETTORE":
            {
                "Premi": {
                    "15.000": "233",
                    "30.000": "271",
                    "50.000": "307",
                    "70.000": "367"
                }
            },
            "CUSTODIA GIUDIZIARIA E VENDITA FORZATA DI IMMOBILI":
            {
                "Premi":{
                    "15.000": "93",
                    "30.000": "108",
                    "50.000": "123",
                    "70.000": "147"
                }
            },
            "AUMENTO DELLA RETROATTIVITÀ DA CINQUE A DIECI ANNI":
            {
                "Premi": "10% aumentato"
            }
        }
    }
}
```
