import React from "react";

import Parse from "parse";

import {Grid, Row, Col, Table, FormControl, FormGroup, Radio, Button} from "react-bootstrap";

import withStyles from "@material-ui/core/styles/withStyles";

import Paper from "@material-ui/core/Paper";

import Typography from "@material-ui/core/Typography";
import FormLabel from "@material-ui/core/FormLabel";
import FileInput from "components/FileInput/FileInput.jsx";

const styles = {
      gridPadding:{
          paddingTop:20,
          paddingBottom:20
      },
      RadioButton: {
        marginRight:10
      },
      importoVisto:{
        width:250
      }
};

var numberOfTables = 0;
class Inserisci extends React.Component{
    constructor(props, context){
        super(props, context);

        this.state = {
           
        }
    }
    createTable = (id) =>{
        const table = [];
        table.push(<tr><th colspan='1'>Fatturato</th><th colspan='7S'>Massimale</th></tr>);
        for(var y = 0; y<8; y++){
          const columns = [];
          for(var x = 0; x<7; x++){
            if(y==0){
              columns.push(<td><FormControl name={id+'-maximal-'+x + "-" + y} class="maximalInput" placeholder="Massimale di.." onChange={this.handleElementChange}></FormControl></td>);
            }else{
              columns.push(<td><FormControl name={id+'-prizes-'+x+'-'+ (y-1)} id={'prizes-'+x+(y-1)} class="premio_lordo" placeholder="Premio lordo..." onChange={this.handleElementChange} onMouseLeave={this.handleOnMouseLeave}></FormControl></td>);
            }  
          }
          if(y!=0){
            table.push(<tr><td class="fatturatoInput"><FormControl name={id+"-earnings-"+(y-1)} placeholder="Fatturato" onChange={this.handleElementChange} onMouseLeave={this.handleOnMouseLeave}></FormControl></td>{columns}</tr>);
          }else{
            table.push(<tr><td></td>{columns}</tr>);
          }
          
        }
        return table;
      }
    
    generateUploadFiles = (id) => {

    }
    ModalElements = () => {
        const modal = [];
        const avvocato = [];
        const commercialista = [];
        const medico = [];
      
        modal.push(<h3>Informazioni generali polizza</h3>);
        modal.push(<div><label>Nome:</label><FormControl name="nome" value={this.state.nome} onChange={this.handleElementChange}></FormControl></div>);
        modal.push(<div><label>Sito web:</label><FormControl name="sito" value={this.state.sito} onChange={this.handleElementChange}></FormControl></div>);
        modal.push(<div><label>Tipo: </label><FormGroup><Radio name="tipo" className={this.props.classes.RadioButton} value="Avvocato" onChange={this.handleElementChange} inline>Avvocato</Radio><Radio className={this.props.classes.RadioButton} value="Commercialista" onChange={this.handleElementChange} name="tipo" inline>Commercialista ed affini</Radio><Radio value="Medico" name="tipo" className={this.props.classes.RadioButton} onChange={this.handleElementChange} inline disabled>Medico ed affini</Radio></FormGroup> </div>)
        avvocato.push(<div>
          <h3>Avvocati in forma individuale senza incarichi di sindaco-revisore-amministratore</h3>
          <Table striped hover condensed bordered>{this.createTable(0)}</Table>
          <h3>Avvocati in forma individuale con incarichi di sindaco-revisore-amministratore fino al 50% del totale</h3>
          <Table striped hover condensed bordered>{this.createTable(1)}</Table>
          <h3>Avvocati in forma collettiva senza incarichi di sindaco-revisore-amministratore</h3>
          <Table striped hover condensed bordered>{this.createTable(2)}</Table>
          <h3>Avvocati in forma collettiva con incarichi di sindaco-revisore-amministratore fino al 50% del totale</h3>
          <Table striped hover condensed bordered>{this.createTable(3)}</Table>
        </div>);

        commercialista.push(
            <div>
                <h3>Commercialisti senza incarichi di sindaco-revisore-amministratore</h3>
                <Table striped hover condensed bordered>{this.createTable(0)}</Table>
                <h3>Commercialisti con incarichi di sindaco-revisore-amministratore fino al 50% del totale</h3>
                <Table striped hover condensed bordered>{this.createTable(1)}</Table>
                <h3>Consulente del lavoro senza incarichi di sindaco-revisore-amministratore</h3>
                <Table striped hover condensed bordered>{this.createTable(2)}</Table>
                <h3>Consulente del lavoro con incarichi di sindaco-revisore-amministratore fino al 50% del totale</h3>
                <Table striped hover condensed bordered>{this.createTable(3)}</Table>
                <h3>Tributarista senza incarichi di sindaco-revisore-amministratore</h3>
                <Table striped hover condensed bordered>{this.createTable(4)}</Table>
                <h3>Tributarista con incarichi di sindaco-revisore-amministratore fino al 50% del totale</h3>
                <Table striped hover condensed bordered>{this.createTable(5)}</Table>
                <h3>Revisore legale senza incarichi di sindaco</h3>
                <Table striped hover condensed bordered>{this.createTable(6)}</Table>
                <h3>Revisore legale con incarichi di sindaco fino al 15% del totale</h3>
                <Table striped hover condensed bordered>{this.createTable(7)}</Table>
                <h3>CED servizi contabili</h3>
                <Table striped hover condensed bordered>{this.createTable(8)}</Table>

                <h3>Visto di conformità</h3>
                <FormGroup><FormLabel>Importo visto di conformità 730: </FormLabel><FormControl name="visto730" value={this.state.visto730} className={this.props.classes.importoVisto}></FormControl></FormGroup>

            </div>
        )
        if(this.state.tipo === "Avvocato"){
          modal.push(avvocato);
          numberOfTables = 4;
        }else if(this.state.tipo === "Commercialista"){
          modal.push(commercialista);
          numberOfTables = 9;
        }
        return modal;
      }
      
      handleSubmit = () => {
        var objSon = {};
        var earnings = [];
        var maximal = [];
        var cachedArrays = {};
        console.log(this.state);
        
        /*
        SCHEMA:
        {
            id:
            {
                [FATTURATO]:
                {
                    [MASSIMALE]: [PREMIO]
                }
            
            }
        }

        y = Fatturato
        x = Massimale

        */
    for(var i = 0; i<numberOfTables; i++){
        cachedArrays[i] = {};
        cachedArrays[i]['maximal'] = [];
        cachedArrays[i]['earnings'] = [];
        objSon[i] = {};
    }
    for(var key in this.state){
        if(key.includes('maximal')){
            const nameArray = key.split('-');
            var id = nameArray[0];
            //var index = parseInt(nameArray[3]);
            const maximalValue = parseInt(this.state[key]);
            //const earningsValue = cachedArrays[i]['earnings'][id];
            //objSon[id][earning[maximalValue] = null;
            //maximal.push(maximalValue);
            cachedArrays[id]['maximal'].push(maximalValue);
        }
        if(key.includes('earnings')){
            const nameArray = key.split('-');
            var id = nameArray[0];
            objSon[id][parseInt(this.state[key])]={};
            //console.log(id);
            //earnings.push(parseInt(this.state[key]));
            var earningsValue = parseInt(this.state[key]);
            cachedArrays[id]['earnings'].push(earningsValue);
        }
      }

      for(var key in this.state){
          if(key.includes('prizes')){
              var parts = key.split('-');
              var id = parts[0];
              var x = parts[2];
              var y = parts[3];
              var earnings = cachedArrays[id]['earnings'][y];
              var maximal = cachedArrays[id]['maximal'][x];
              console.log(cachedArrays[id]['earnings'][0]);
              console.log(id);
              objSon[id][earnings][maximal] = parseInt(this.state[key]);
              //console.log(objSon[id][earnings]);
          }
      }
      
      var nome = this.state.nome;
      var website = this.state.sito;
      var tipo = this.state.tipo;
      
      var PolizzaClass = Parse.Object.extend('Insurances');
      var PolizzaObj = new PolizzaClass();
      PolizzaObj.set('nome', nome);
      PolizzaObj.set('website', website);
      PolizzaObj.set('tipo', tipo);
      PolizzaObj.set('payload', objSon);
      if(this.state.tipo === "Commercialista"){
        PolizzaObj.set('visto_conformita', this.state.visto_conformita);
      }
      PolizzaObj.save({useMasterKey:true}).then((PolizzaObj)=>{
        alert("Polizza salvata con successo");
      });
      console.log(this.state);

    }
      
      closeCallback = () => {
        console.log(this.state);
      }
      
      handleShow = () => {
      
      }
      handleElementChange = (event)=>{
        const targ = event.target;
        const name = targ.name;
        const value = targ.value;
        this.setState({
           [name]: value
        });
      
        console.log(this.state);
        
      }
      handleOnMouseLeave = (event) =>{
          const target = event.target;
          const name = target.name;
          const value = target.value;
          const id = name.split('-')[0]

          if(name.indexOf("maximal")>-1 && value != ""){
            
            this.state.base[value]=[];
          }else if(name.indexOf("prizes")>-1 && value != "" && id != undefined){
            var parts = name.split('-');
            var x=parts[1];
            var y=parts[2];
            const earnings = {};
            //earnings[this.state.earnings[id][y]] = value;
            //this.state.base[arrays['maximal'][id][x]].push(earnings);
          }else if(name.indexOf("earnings")>-1 && value != ""){
            //this.state.earnings[id].push(parseInt(value));
          }
      }

      handleOnMouseOut = (event) => {
          console.log(event.target.name);
      }

      getInsurances = (event) => {
          var payload = {professione: 'Commercialista', fatturato: null, massimale: null};
          var request = new XMLHttpRequest();
          request.open('POST', 'http://192.168.1.96:3434/insurance/getBestInsurance', true);
          request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
          request.send(JSON.stringify(payload));
      }
      render(){
          return(
              <div>
              <Paper>
              <Grid className={this.props.classes.gridPadding}>
              {this.ModalElements()}
              <Button bsStyle="success" onClick={this.handleSubmit} className={this.props.classes.submit} on>Inserisci polizza</Button>
              <Button bsStyle="success" onClick={this.getInsurances}>Richiedi polizze</Button>
              </Grid>
              </Paper>
              </div>
          );
      }
}
export default withStyles(styles)(Inserisci);